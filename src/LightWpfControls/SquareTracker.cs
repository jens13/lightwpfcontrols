﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Windows;

namespace LightWpfControls
{
    public class SquareTracker : TrackerBase
    {
        static SquareTracker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof (SquareTracker),
                                                     new FrameworkPropertyMetadata(typeof (SquareTracker)));
        }

        protected override Point CalculateMarkerPosition(Point point)
        {
            double x = point.X;
            double y = point.Y;
            double absX = Math.Abs(x);
            double absY = Math.Abs(y);
            if (absX > RadiusOuterShape || absY > RadiusOuterShape)
            {
                if (absX > absY)
                {
                    x = Math.Sign(x)*RadiusOuterShape;
                    y = Math.Sign(y)*RadiusOuterShape*absY/absX;
                }
                else if (absX < absY)
                {
                    x = Math.Sign(x)*RadiusOuterShape*absX/absY;
                    y = Math.Sign(y)*RadiusOuterShape;
                }
                else
                {
                    x = Math.Sign(x)*RadiusOuterShape*absX/absY;
                    y = Math.Sign(y)*RadiusOuterShape;
                }
            }
            x += MarkerCenterPositionX;
            y += MarkerCenterPositionY;
            double length = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
            var markerPosition = new Point(x, y);
            x = x - MarkerCenterPositionX;
            y = MarkerCenterPositionY - y;
            Position = new Point(x, y);
            Vector = length*MaxMinDelta + MinimumValue;
            InternalValue = new Point(Math.Sign(x)*(Math.Abs(x)*MaxMinDelta + MinimumValue),
                                      Math.Sign(y)*(Math.Abs(y)*MaxMinDelta + MinimumValue));
            return markerPosition;
        }
    }
}