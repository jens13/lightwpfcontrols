﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.Reflection;
using System.Windows;

[assembly: AssemblyTitle("LightWpfControls")]
[assembly: AssemblyDescription("Some light WPF controls...")]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]