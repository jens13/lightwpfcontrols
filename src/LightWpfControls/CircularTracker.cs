﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Windows;

namespace LightWpfControls
{
    public class CircularTracker : TrackerBase
    {
        static CircularTracker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof (CircularTracker),
                                                     new FrameworkPropertyMetadata(typeof (CircularTracker)));
        }

        protected override Point CalculateMarkerPosition(Point point)
        {
            double length = Math.Sqrt(Math.Pow(point.X, 2) + Math.Pow(point.Y, 2));
            double x = point.X + MarkerCenterPositionX;
            double y = point.Y + MarkerCenterPositionY;
            if (length > RadiusOuterShape)
            {
                x = point.X*RadiusOuterShape/length + MarkerCenterPositionX;
                y = point.Y*RadiusOuterShape/length + MarkerCenterPositionY;
            }
            var markerPosition = new Point(x, y);
            x = x - MarkerCenterPositionX;
            y = MarkerCenterPositionY - y;
            Position = new Point(x, y);
            Vector = (length > RadiusOuterShape ? RadiusOuterShape : length)*MaxMinDelta + MinimumValue;
            InternalValue = new Point(Math.Sign(x)*(Math.Abs(x)*MaxMinDelta + MinimumValue),
                                      Math.Sign(y)*(Math.Abs(y)*MaxMinDelta + MinimumValue));
            return markerPosition;
        }
    }
}