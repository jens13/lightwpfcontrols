﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace LightWpfControls
{
    public abstract class TrackerBase : Control
    {
        public delegate void TrackerEventHandler
            (object sender, TrackerEventArgs e);

        public static readonly DependencyProperty BackgroundColorProperty =
            DependencyProperty.Register("BackgroundColor", typeof (Color), typeof (TrackerBase),
                                        new UIPropertyMetadata(Colors.Black));

        public static readonly DependencyProperty MarkerColor1Property =
            DependencyProperty.Register("MarkerColor1", typeof (Color), typeof (TrackerBase),
                                        new UIPropertyMetadata(Colors.White));

        public static readonly DependencyProperty MarkerColor2Property =
            DependencyProperty.Register("MarkerColor2", typeof (Color), typeof (TrackerBase),
                                        new UIPropertyMetadata(Colors.Black));

        public static readonly DependencyProperty CrosshairThicknessProperty =
            DependencyProperty.Register("CrosshairThickness", typeof (int), typeof (TrackerBase),
                                        new UIPropertyMetadata(1));

        public static readonly DependencyProperty KeepMarkerPositionProperty =
            DependencyProperty.Register("KeepMarkerPosition", typeof (bool), typeof (TrackerBase),
                                        new UIPropertyMetadata(false));

        public static readonly DependencyProperty MinimumValueProperty =
            DependencyProperty.Register("MinimumValue", typeof (double), typeof (TrackerBase),
                                        new UIPropertyMetadata(0.0));

        public static readonly DependencyProperty MaximumValueProperty =
            DependencyProperty.Register("MaximumValue", typeof (double), typeof (TrackerBase),
                                        new UIPropertyMetadata(100.0));

        public static readonly DependencyProperty MarkerSizeProperty =
            DependencyProperty.Register("MarkerSize", typeof (double), typeof (TrackerBase),
                                        new UIPropertyMetadata(25.0));

        public static readonly RoutedEvent ValueChangedEvent =
            EventManager.RegisterRoutedEvent
                ("ValueChanged", RoutingStrategy.Bubble,
                 typeof(TrackerEventHandler), typeof(TrackerBase));

        public static readonly RoutedEvent MarkerReleasedEvent =
            EventManager.RegisterRoutedEvent
                ("MarkerReleased", RoutingStrategy.Bubble,
                 typeof(TrackerEventHandler), typeof(TrackerBase));

        public static readonly RoutedEvent MarkerReturnedEvent =
            EventManager.RegisterRoutedEvent
                ("MarkerReturned", RoutingStrategy.Bubble,
                 typeof(TrackerEventHandler), typeof(TrackerBase));

        protected double CenterX;
        protected double CenterY;
        protected Point InternalValue;
        protected double MarkerCenterPositionX;
        protected double MarkerCenterPositionY;
        protected double MaxMinDelta;
        protected double Radius;
        protected double RadiusInnerShape;
        protected double RadiusMarker = 12;
        protected double RadiusOuterShape;
        private Ellipse _center;
        private Point _centerPosition;
        private Line _crosshairHorizontal;
        private Shape _crosshairInnerShape;
        private Shape _crosshairOuterShape;
        private Line _crosshairVertical;
        private Grid _marker;
        private Ellipse _mover;
        private bool _moverIsMoving;
        private Shape _movmentArea;
        private bool _renderCompleted;
        private bool _mouseDoubleClicked;
        private bool _mouseHasMoved;

        static TrackerBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof (TrackerBase),
                                                     new FrameworkPropertyMetadata(typeof (TrackerBase)));
            BorderBrushProperty.AddOwner(typeof (TrackerBase),
                                         new FrameworkPropertyMetadata(new SolidColorBrush(Colors.Black)));
            BorderThicknessProperty.AddOwner(typeof (TrackerBase), new FrameworkPropertyMetadata(new Thickness(1)));
        }


        public int CrosshairThickness { get { return (int) GetValue(CrosshairThicknessProperty); } set { SetValue(CrosshairThicknessProperty, value); } }

        public bool KeepMarkerPosition { get { return (bool) GetValue(KeepMarkerPositionProperty); } set { SetValue(KeepMarkerPositionProperty, value); } }

        public double MinimumValue { get { return (double) GetValue(MinimumValueProperty); } set { SetValue(MinimumValueProperty, value); } }

        public double MaximumValue { get { return (double) GetValue(MaximumValueProperty); } set { SetValue(MaximumValueProperty, value); } }

        public Color BackgroundColor { get { return (Color) GetValue(BackgroundColorProperty); } set { SetValue(BackgroundColorProperty, value); } }

        public Color MarkerColor1 { get { return (Color) GetValue(MarkerColor1Property); } set { SetValue(MarkerColor1Property, value); } }
        public Color MarkerColor2 { get { return (Color) GetValue(MarkerColor2Property); } set { SetValue(MarkerColor2Property, value); } }

        public double MarkerSize { get { return (double) GetValue(MarkerSizeProperty); } set { SetValue(MarkerSizeProperty, value); } }
        public double MarkerMargin { get { return ((double) GetValue(MarkerSizeProperty))/10.0 + 2; } }
        public double HalfMarkerSize { get { return ((double) GetValue(MarkerSizeProperty))/2; } }

        /// <summary>
        ///     Gets X,Y coordinate in pixels
        /// </summary>
        public Point Position { get; protected set; }

        /// <summary>
        ///     Gets or sets calculated X,Y values based on MinimumValue and MaximumValue
        /// </summary>
        public Point Value
        {
            get { return InternalValue; }
            set
            {
                if (InternalValue == value) return;
                InternalValue = value;
                if (!_renderCompleted) return;
                ValueToPosition();
            }
        }

        /// <summary>
        ///     Gets calculated length of the vector based on MinimumValue and MaximumValue
        /// </summary>
        public double Vector { get; protected set; }

        protected override Size MeasureOverride(Size constraint)
        {
            double x = 200;
            double y = 200;
            if (double.IsInfinity(constraint.Width) && !double.IsInfinity(constraint.Height))
            {
                x = constraint.Height;
                y = constraint.Height;
            }
            else if (double.IsInfinity(constraint.Height) && !double.IsInfinity(constraint.Width))
            {
                x = constraint.Width;
                y = constraint.Width;
            }
            else if (!double.IsInfinity(constraint.Height) && !double.IsInfinity(constraint.Width))
            {
                x = constraint.Width;
                y = constraint.Height;
            }

            return new Size(x, y);
        }

        public override void OnApplyTemplate()
        {
            InitializeTemplateChildren();

            base.OnApplyTemplate();
        }

        private void InitializeTemplateChildren()
        {
            _movmentArea = GetTemplateChild("PART_MovmentArea") as Shape;
            _crosshairInnerShape = GetTemplateChild("PART_CrosshairInnerShape") as Shape;
            _crosshairOuterShape = GetTemplateChild("PART_CrosshairOuterShape") as Shape;
            _center = GetTemplateChild("PART_Center") as Ellipse;
            _marker = GetTemplateChild("PART_Marker") as Grid;
            _mover = GetTemplateChild("PART_Mover") as Ellipse;
            _crosshairHorizontal = GetTemplateChild("PART_CrosshairHorizontal") as Line;
            _crosshairVertical = GetTemplateChild("PART_CrosshairVertical") as Line;

            if (_mover != null)
            {
                _mover.MouseLeftButtonDown += MoverOnMouseLeftButtonDown;
                _mover.MouseLeftButtonUp += MoverOnMouseLeftButtonUp;
                _mover.MouseMove += MoverOnMouseMove;
            }
            if (_marker != null)
            {
                _marker.MouseLeftButtonDown += MoverOnMouseLeftButtonDown;
                _marker.MouseLeftButtonUp += MoverOnMouseLeftButtonUp;
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            PrepareCalculationValues();

            PositionShapes();

            _renderCompleted = true;
            if (KeepMarkerPosition && InternalValue != new Point()) ValueToPosition();

            base.OnRender(drawingContext);
        }

        protected virtual void PrepareCalculationValues()
        {
            RadiusMarker = MarkerSize/2.0;
            CenterX = ActualWidth/2.0;
            CenterY = ActualHeight/2.0;
            Radius = Math.Min(CenterX, CenterY);
            RadiusOuterShape = Radius - RadiusMarker - BorderThickness.Top;
            RadiusInnerShape = RadiusOuterShape/2.0 + BorderThickness.Top;
            MarkerCenterPositionX = CenterX - RadiusMarker;
            MarkerCenterPositionY = CenterY - RadiusMarker;
            MaxMinDelta = (MaximumValue - MinimumValue)/RadiusOuterShape;
            _centerPosition = new Point(MarkerCenterPositionX, MarkerCenterPositionY);
        }

        private void PositionShapes()
        {
            SetPosition(_mover, _centerPosition);
            SetPosition(_marker, _centerPosition);
            SetPosition(_center, CenterX, CenterY);
            SetPosition(_movmentArea, CenterX - Radius, CenterY - Radius);

            double size = Math.Min(ActualWidth, ActualHeight);
            _movmentArea.Width = size;
            _movmentArea.Height = size;

            int crosshairThickness = CrosshairThickness;
            if (crosshairThickness > 0)
            {
                _crosshairInnerShape.Width = RadiusOuterShape;
                _crosshairInnerShape.Height = RadiusOuterShape;
                SetPosition(_crosshairInnerShape, CenterX - RadiusInnerShape, CenterY - RadiusInnerShape);
                _crosshairOuterShape.Width = RadiusOuterShape*2 + crosshairThickness;
                _crosshairOuterShape.Height = RadiusOuterShape*2 + crosshairThickness;
                SetPosition(_crosshairOuterShape, CenterX - RadiusOuterShape, CenterY - RadiusOuterShape);

                _crosshairHorizontal.X1 = CenterX - Radius;
                _crosshairHorizontal.X2 = CenterX + Radius;
                _crosshairHorizontal.Y1 = CenterY;
                _crosshairHorizontal.Y2 = CenterY;
                _crosshairVertical.X1 = CenterX;
                _crosshairVertical.X2 = CenterX;
                _crosshairVertical.Y1 = CenterY - Radius;
                _crosshairVertical.Y2 = CenterY + Radius;
            }
        }

        private void MoverOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                MoverOnDoubleClick(sender, e);
            }
            else
            {
                BeginMoveMover();
            }
        }

        private void MoverOnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!_mouseHasMoved) return;
            EndMoveMover();
        }

        private void MoverLostMouseCapture(object sender, MouseEventArgs e)
        {
            _mover.LostMouseCapture -= MoverLostMouseCapture;
            if (!_mouseHasMoved) return;
            EndMoveMover();
        }

        private void MoverOnDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!KeepMarkerPosition) return;
            _mouseDoubleClicked = true;
            EndMoveMover();
        }

        protected void BeginMoveMover()
        {
            if (_moverIsMoving) return;
            if (_mover.CaptureMouse())
            {
                _mover.LostMouseCapture += MoverLostMouseCapture;
                _moverIsMoving = true;
            }
        }

        private void MoverOnMouseMove(object sender, MouseEventArgs e)
        {
            if (!_moverIsMoving) return;
            _mouseHasMoved = true;
            Point point = e.GetPosition(_center);
            CalculateMarkerPosition(point);
            Point markerPosition = CalculateMarkerPosition(point);
            SetPosition(_marker, markerPosition);
            SetPosition(_mover, point);
            RaiseValueChangedEvent();
        }

        protected void EndMoveMover()
        {
            _mouseHasMoved = false;
            _moverIsMoving = false;
            _mover.ReleaseMouseCapture();
            if (!KeepMarkerPosition || _mouseDoubleClicked)
            {
                _mouseDoubleClicked = false;
                ReturnMarker();
                RaiseMarkerReturnedChangedEvent();
            }
            else
            {
                RaiseMarkerReleasedChangedEvent();
            }
        }

        protected void ValueToPosition()
        {
            double min = MinimumValue;
            if (InternalValue.X <= min && InternalValue.X >= -min && InternalValue.Y <= min && InternalValue.Y >= -min)
            {
                ReturnMarker();
                RaiseValueChangedEvent();
                return;
            }
            var point = new Point((InternalValue.X - min)/MaxMinDelta,
                                  -(InternalValue.Y - min)/MaxMinDelta);
            Point markerPosition = CalculateMarkerPosition(point);
            SetPosition(_marker, markerPosition);
            SetPosition(_mover, markerPosition);
            RaiseValueChangedEvent();
        }

        protected void ReturnMarker()
        {
            SetPosition(_mover, _centerPosition);
            SetPosition(_marker, _centerPosition);
            double min = MinimumValue;
            InternalValue = new Point(min, min);
            Vector = min;
            Position = new Point(0.0, 0.0);
        }

        protected void SetPosition(DependencyObject dependencyObject, double x, double y)
        {
            dependencyObject.SetValue(Canvas.LeftProperty, x);
            dependencyObject.SetValue(Canvas.TopProperty, y);
        }

        protected void SetPosition(DependencyObject dependencyObject, Point position)
        {
            dependencyObject.SetValue(Canvas.LeftProperty, position.X);
            dependencyObject.SetValue(Canvas.TopProperty, position.Y);
        }


        public event TrackerEventHandler ValueChanged { add { AddHandler(ValueChangedEvent, value); } remove { RemoveHandler(ValueChangedEvent, value); } }
        public event TrackerEventHandler MarkerReleased { add { AddHandler(MarkerReleasedEvent, value); } remove { RemoveHandler(MarkerReleasedEvent, value); } }
        public event TrackerEventHandler MarkerReturned { add { AddHandler(MarkerReturnedEvent, value); } remove { RemoveHandler(MarkerReturnedEvent, value); } }

        protected void RaiseValueChangedEvent()
        {
            var newEventArgs = new TrackerEventArgs(Vector, InternalValue, Position, ValueChangedEvent);
            RaiseEvent(newEventArgs);
        }

        protected void RaiseMarkerReleasedChangedEvent()
        {
            var newEventArgs = new TrackerEventArgs(Vector, InternalValue, Position, MarkerReleasedEvent);
            RaiseEvent(newEventArgs);
        }

        protected void RaiseMarkerReturnedChangedEvent()
        {
            var newEventArgs = new TrackerEventArgs(Vector, InternalValue, Position, MarkerReturnedEvent);
            RaiseEvent(newEventArgs);
        }

        protected abstract Point CalculateMarkerPosition(Point point);
    }
}