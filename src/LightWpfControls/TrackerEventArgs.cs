﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.Windows;

namespace LightWpfControls
{
    public class TrackerEventArgs : RoutedEventArgs
    {
        public TrackerEventArgs(double vector, Point value, Point position)
        {
            Position = position;
            Value = value;
            Vector = vector;
        }

        public TrackerEventArgs(double vector, Point value, Point position, RoutedEvent routedEvent)
            : base(routedEvent)
        {
            Position = position;
            Value = value;
            Vector = vector;
        }

        /// <summary>
        ///     X,Y coordinate in pixels
        /// </summary>
        public Point Position { get; private set; }

        /// <summary>
        ///     Calculated X,Y values based on MinimumValue and MaximumValue
        /// </summary>
        public Point Value { get; private set; }

        /// <summary>
        ///     Calculated length of the vector based on MinimumValue and MaximumValue
        /// </summary>
        public double Vector { get; private set; }
    }
}