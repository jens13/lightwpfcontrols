﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("LightWpfControls")]
[assembly: AssemblyProduct("LightWpfControls")]
[assembly: AssemblyCopyright("Copyright © Jens Granlund 2012")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.0.1.0")]
[assembly: AssemblyFileVersion("1.0.1.0")]
[assembly: AssemblyInformationalVersion("1.0.1.0")]