﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.Windows;
using System.Windows.Input;

namespace LightWpfControls.Demo
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Tracker2.Value = new Point(50, 50);
        }

        private void UpdatePosition(TrackerEventArgs e)
        {
            TestLabel.Content =
                string.Format("Value {0:0.##} x {1:0.##} -- Position {2:0.##} x {3:0.##} -- Length {4:0.##}", e.Value.X,
                              e.Value.Y, e.Position.X,
                              e.Position.Y, e.Vector);
        }

        private void TestLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Tracker1.MaximumValue = 20;
        }

        private void Tracker1_OnMarkerMoved(object sender, TrackerEventArgs e)
        {
            UpdatePosition(e);
        }

        private void Tracker4_OnMarkerReturned(object sender, TrackerEventArgs e)
        {
            UpdatePosition(e);
            TestLabel.Content += " -- Returned";
        }

        private void Tracker2_OnMarkerReleased(object sender, TrackerEventArgs e)
        {
            UpdatePosition(e);
            TestLabel.Content += " -- Released";
        }
    }
}